"""Models a zone.
A zone is a collection of blocks with integer coordinates. Each block has the
following properties:
 - id Integer corresponding to theid of the mesh that is represented by the block.
 - rotation Integer corresponding to the rotation of the block. 0 is north and
	 goes clockwise to 3.
 - position Vector3 of integer coordinates.
"""

var entry_points setget ,get_entry_points
var blocks setget ,get_blocks

func _init(zone_dictionary):
	blocks = []
	for block_dictionary in zone_dictionary["blocks"]:
		var block = {}
		block["id"] = block_dictionary["id"]
		block["rotation"] = block_dictionary["rotation"]
		var position = block_dictionary["position"]
		block["position"] = Vector3(position[0],position[1],position[2])
		blocks.append(block)

	entry_points = []
	for entry_point_dictionary in zone_dictionary["entry_points"]:
		var entry_point = {}
		var position = entry_point_dictionary["position"]
		entry_point["position"] = Vector3(position[0],position[1],position[2])
		entry_point["rotation"] = entry_point_dictionary["rotation"]
		entry_points.append(entry_point)

func get_entry_points():
	return entry_points

func get_blocks():
	return blocks

func rotate_point(point, direction):
	if direction == 0: return point
	if direction == 1: return Vector3(   point[2],point[1],-1*point[0])
	if direction == 2: return Vector3(-1*point[0],point[1],-1*point[2])
	if direction == 3: return Vector3(-1*point[2],point[1],   point[0])

func translate_entry_point(entry_point_index,position,rotation):
	"""
	Changes every coordinate so that the specified entry point is now at position
	pointing in the specified rotation.
	As the entry_point can have multiple rotations the entry_rotation is the index
	to the desired entry rotation.
	"""
	# Rotate position
	var entry_point = entry_points[entry_point_index]
	var needed_rotation = int(rotation-entry_point.rotation)%4
	if needed_rotation < 0:
		needed_rotation += 4
	for point in blocks + entry_points:
		point.position = rotate_point(point.position, needed_rotation)
	
	# Set rotation
	for point in blocks + entry_points:
		point.rotation = int(point.rotation + needed_rotation)%4

	# Move entry_point to desired position
	var to_origin = entry_point.position*-1 + position
	for point in blocks + entry_points:
		point.position += to_origin

func entry_point_to_exit_point(entry_point_index):
	var entry_point = entry_points[entry_point_index].duplicate()
	entry_point.position += rotate_point(Vector3(1,0,0),(entry_point.rotation+2)%4)
	entry_point.rotation = (entry_point.rotation+2)%4
	return entry_point

func fits(grid,starting_position):
	var used_cells = grid.get_used_cells()
	for block in blocks:
		for i in range(-2,3):
			if Vector3(0,i,0) + block.position in used_cells: return false
	return true
