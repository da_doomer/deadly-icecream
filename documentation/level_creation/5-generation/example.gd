extends WorldEnvironment

var ProceduralLevel = load("res://level-generation/procedural-level-generator.gd")
var level_library_path = "res://level-generation/level-library"
var player_scene = load("res://character/player.tscn")

func _ready():
	var light = DirectionalLight.new()
	add_child(light)
	light.shadow_enabled = true
	light.rotate(Vector3(1,0,0),-0.78)
	light.directional_shadow_mode = DirectionalLight.SHADOW_PARALLEL_2_SPLITS
	light.light_bake_mode = DirectionalLight.BAKE_ALL

	environment = load(level_library_path + "/environment.tres")

	var level_danger = 20
	var level_loot = 10
	var level = ProceduralLevel.new(level_library_path,level_danger,level_loot)
	add_child(level)

	var player = player_scene.instance()
	add_child(player)
	var player_offset_to_start = player.to_local(level.get_start_position())
	player.translate(player_offset_to_start)
