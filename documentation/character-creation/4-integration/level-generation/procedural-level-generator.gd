extends Spatial

var Zone = preload("zone.gd")
var gridmap setget ,get_gridmap
var start_position setget ,get_start_position

func get_start_position():
	return gridmap.map_to_world(0,0,0)

func get_gridmap():
	return gridmap

func _init(level_library_path, danger, loot):
	randomize()

	# Open the mesh library
	var mesh_library_path = level_library_path + "/block-library.res"
	var mesh_library = load(mesh_library_path)

	# Create the zone library
	var zone_library_path = level_library_path + "/zone-library.json"
	var file = File.new()
	file.open(zone_library_path,file.READ)
	var zone_library_json = JSON.parse(file.get_as_text())
	var zone_library = []
	for zone_dictionary in zone_library_json.result:
		zone_library.append(Zone.new(zone_dictionary))

	var length = danger*20
	gridmap = create_random_gridmap(mesh_library, zone_library, length)

func rotation_to_gridmap_rotation(rotation):
	if rotation == 0: return 0
	if rotation == 1: return 16
	if rotation == 2: return 10
	if rotation == 3: return 22

func create_random_gridmap(mesh_library, zone_library, length):
	var grid = GridMap.new()
	grid.set_name("gridmap")
	grid.theme = mesh_library
	grid.cell_size= Vector3(1,1,1)

	var remaining_starts = [ {"position":Vector3(0,0,0),"rotation":0} ]
	var current_length = 0
	var loops = 0

	while current_length < length and remaining_starts.size() > 0 and loops < length*100:
		loops += 1
		# Pick a starting point
		var start_index = randi()%remaining_starts.size()
		var possible = remaining_starts[start_index].position

		# If start is already used, mark it as used and try again
		if possible in grid.get_used_cells():
			remaining_starts.remove(start_index)
			continue

		var current_position = remaining_starts[start_index].position
		var current_rotation = remaining_starts[start_index].rotation

		# Choose a zone
		var zone_index = randi()%zone_library.size()
		var zone = zone_library[zone_index]
		# Choose entry and exit points
		var entry_index = randi()%zone.get_entry_points().size()
		# Translate the zone entry point to the current position and rotation
		zone.translate_entry_point(entry_index,current_position,current_rotation)

		if not zone.fits(grid,current_position): continue

		# Mark the start as used
		remaining_starts.remove(start_index)

		# Put each block from the zone in the gridmap
		for block in zone.get_blocks():
			var position = block.position
			var rotation = rotation_to_gridmap_rotation(block.rotation)
			var id = block["id"]
			grid.set_cell_item(position[0], position[1], position[2], id, rotation)
		current_length += zone.get_blocks().size()

		# Add exit points to the possible starting points
		for exit_index in range(zone.get_entry_points().size()):
			if exit_index != entry_index:
				var exit_point = zone.entry_point_to_exit_point(exit_index)
				remaining_starts.append(exit_point)

	return grid

func _ready():
	add_child(gridmap)
	gridmap.make_baked_meshes()
